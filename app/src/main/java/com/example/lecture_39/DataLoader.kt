package com.example.lecture_39

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

object DataLoader {
    private const val BASE_URL = "https://maps.googleapis.com/maps/api/"
    const val AUTOCOMPLETE = "autocomplete"

    private val retrofit =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(ScalarsConverterFactory.create())
            .build()

    private val service = retrofit.create(APIService::class.java)

    fun getRequest(path: String, parameters: MutableMap<String, String>, callback: FutureCallback) {
        val call = service.getRequest(path, parameters)
        call.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                callback.onFailure(t.message.toString())
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                callback.onResponse(response.body().toString())
            }

        })
    }

    interface APIService {
        @GET("place/{path}/json")
        fun getRequest(
            @Path("path") path: String,
            @QueryMap parameters: Map<String, String>
        ): Call<String>
    }

}