package com.example.lecture_39

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    companion object {
        const val API_KEY = "AIzaSyBADWUmhO9XNVF_-qSZR6RQWcoHfSpAr6E"
    }

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter
    private val items = mutableListOf<SearchModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        recyclerViewAdapter = RecyclerViewAdapter(items)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = recyclerViewAdapter
        searchEditText.addTextChangedListener(textWatcher)
    }

    private fun getAddresses(input: String) {
        if (input.isNotEmpty()) {
            progressBar.visibility = View.VISIBLE
            items.clear()
            val parameters = mutableMapOf<String, String>()
            parameters["input"] = input
            parameters["key"] = API_KEY
            DataLoader.getRequest(DataLoader.AUTOCOMPLETE, parameters, object : FutureCallback {
                override fun onResponse(response: String) {
                    val jsonResponse = JSONObject(response)
                    if (jsonResponse.has("predictions") && !jsonResponse.has("error_message")) {
                        val jsonPredictions = jsonResponse.getJSONArray("predictions")
                        (0 until jsonPredictions.length()).forEach {
                            val jsonPrediction = jsonPredictions.getJSONObject(it)
                            items.add(
                                SearchModel(
                                    jsonPrediction.getString("description"),
                                    jsonPrediction.getString("place_id")
                                )
                            )
                        }
                        progressBar.visibility = View.GONE
                        recyclerViewAdapter.notifyDataSetChanged()
                    } else  {
                        onFailure(jsonResponse.getString("error_message"))
                    }
                }

                override fun onFailure(error: String) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this@MainActivity, error, Toast.LENGTH_SHORT).show()
                }

            })
        }

    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            getAddresses(s.toString())
        }

    }
}
