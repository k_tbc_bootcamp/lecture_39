package com.example.lecture_39

interface FutureCallback {
    fun onResponse(response: String)
    fun onFailure(error: String)
}